#! /usr/bin/env python
# -- coding: utf-8

""" Hget - Extensible, modular, multithreaded Internet
    downloader program in the spirit of wget, using
    HarvestMan codebase, with HTTP multipart support.
    
    Version      - 1.0 beta 1.

    Author: Anand B Pillai <abpillai at gmail dot com>

    HGET is free software. See the file LICENSE.txt for information
    on the terms and conditions of usage, and a DISCLAIMER of ALL WARRANTIES.

 Modification History

    Created: April 19 2007 Anand B Pillai

     April 20 2007 Added more command-line options   Anand
     April 24 2007 Made connector module to flush data  Anand
                   to tempfiles when run with hget.
     April 25 2007 Implementation of hget features is  Anand
                   completed!
     April 30 2007 Many fixes/enhancements to hget.
                   1. Reconnection of a lost connection does not
                   lose already downloaded data.
                   2. Closing of files of threads when download is
                   aborted.
                   3. Thread count shows current number of threads
                   which are actually doing downloads, reflecting
                   the activity.
                   4. Final printing of time taken, average bandwidth
                   and file size.

     May 10 2007   Added support for sf.net mirrors in multipart.
     Aug    2007   Fixed bugs in resetting state of various objects
                   when doing many multipart downloads one after other.
     
Copyright(C) 2007, Anand B Pillai

"""

import __init__
import sys, os
import re
import shutil


from lib import connector
from lib import urlparser
from lib import config
from lib import logger
from lib import datamgr
from lib import urlthread
from lib import mirrors

from lib.common.common import *
from lib.common.macros import *

from harvestman import HarvestMan

VERSION='1.0'
MATURITY='beta 1'

class Hget(HarvestMan):
    """ Web getter class for HarvestMan which defines
    a wget like interface for downloading files on the
    command line with HTTP/1.0 Multipart support, mirror
    search and failover """

    USER_AGENT = "HarvestMan v2.0"

    def grab_url(self, url, filename=None):
        """ Download URL """

        # If a filename is given, set outfile to it
        if filename:
            self._cfg.hgetoutfile = filename
            # print 'Saving to',filename

        # We need to reset some counters and
        # data structures ...
        
        # Reset progress object
        self._cfg.reset_progress()
        # Reset thread pool, multipart status
        self._pool.reset_multipart_data()
        # Reset monitor
        self._monitor.reset()
        # Reset mirror manager
        mirrormgr = mirrors.HarvestManMirrorManager.getInstance()
        mirrormgr.reset()
        
        try:
            # print self._cfg.requests, self._cfg.connections
            conn = connector.HarvestManUrlConnector()
            urlobj = None
            
            try:
                print '\nDownloading URL',url,'...'
                urlobj = urlparser.HarvestManUrlParser(url)
                ret = conn.url_to_file(urlobj)

                if urlobj.trymultipart and mirrormgr.used:
                    # Print stats if mirrors were used...
                    mirrormgr.print_stats()
                    
                return HGET_DOWNLOAD_OK
            except urlparser.HarvestManUrlParserError, e:
                print str(e)
                print 'Error: Invalid URL "%s"' % url

                return HGET_DOWNLOAD_ERROR
            
        except KeyboardInterrupt, e:
            print 'Caught keyboard interrupt...'
            if urlobj: self.clean_up(conn, urlobj)

            return HGET_KEYBOARD_INTERRUPT

        except Exception, e:
            print 'Caught fatal error (%s): %s' % (e.__class__.__name__, str(e))
            if urlobj: self.clean_up(conn, urlobj, e)
            print_traceback()

            return HGET_FATAL_ERROR
            
    def clean_up(self, conn, urlobj, exception=None):

        reader = conn.get_reader()
        if reader: reader.stop()
        if exception==None:
            print '\n\nDownload aborted by user interrupt.'

        # If flushdata mode, delete temporary files
        if self._cfg.datamode == CONNECTOR_DATA_MODE_FLUSH:
            print 'Cleaning up temporary files...'
            fname1 = conn.get_tmpfname()
            # print 'Temp fname=>',fname1
            
            fullurl = urlobj.get_full_url()
            range_request = conn._headers.get('accept-ranges','').lower()
            # If server supports range requests, then do not
            # clean up temp file, since we can start from where
            # we left off, if this file is requested again.
            if not range_request=='bytes':
                if fname1:
                    try:
                        os.remove(fname1)
                    except OSError, e:
                        print e
            elif fname1:
                # Dump an info file consisting of the header
                # information to a file, so that we can use it
                # to resume downloading from where we left off
                conn.write_url_info_file(fullurl)

            lthreads = self._pool.get_threads()
            lfiles = []
            for t in lthreads:
                fname = t.get_tmpfname()
                if fname: lfiles.append(fname)
                t.close_file()

            print 'Waiting for threads to finish...'
            self._pool.end_all_threads()

            # For currently running multipart download, clean
            # up all pieces since there is no guarantee that
            # the next request will be for the same number of
            # pieces of files, though the server supports
            # multipart downloads.
            if lfiles:
                tmpdir = os.path.dirname(lfiles[0])
            else:
                tmpdir = ''
                
            for f in lfiles:
                if os.path.isfile(f):
                    try:
                        os.remove(f)
                    except (IOError, OSError), e:
                        print 'Error: ',e

            # Commented out because this is giving a strange
            # exception on Windows.
            
            # If doing multipart, cleanup temp dir also
            if self._cfg.multipart:
                if not self._cfg.hgetnotemp and tmpdir:
                    try:
                        shutil.rmtree(tmpdir)
                    except OSError, e:
                        print e
            print 'Done'

        print ''
        
    def create_initial_directories(self):
        """ Create the initial directories for Hget """

        super(Hget, self).create_initial_directories()
        # Create temporary directory for saving files
        if not self._cfg.hgetnotemp:
            try:
                tmp = GetMyTempDir()
                if not os.path.isdir(tmp):
                    os.makedirs(tmp)
                # Could not make tempdir, set hgetnotemp to True
                if not os.path.isdir(tmp):
                    self._cfg.hgetnotemp = True
            except Exception, e:
                pass

    def init(self):
        """ Do the basic things and get ready """

        # Set logging format to plain
        GetObject('logger').setPlainFormat()

        self._cfg.USER_AGENT = self.__class__.USER_AGENT
        # Fudge Firefox USER-AGENT string since some sites
        # dont accept our user-agent.
        # self._cfg.USER_AGENT = "Firefox/2.0.0.8"
        self._cfg.appname = 'Hget'
        self._cfg.version = VERSION
        self._cfg.maturity = MATURITY
        self._cfg.nocrawl = True
        # For hget, default data mode is flush
        self._cfg.datamode = CONNECTOR_DATA_MODE_FLUSH
        self._pool = None
        self._monitor = None
        
        # Get program options
        self._cfg.parse_arguments()
        
        self._cfg.threadpoolsize = 20
        # Set number of connections to two plus numparts
        self._cfg.connections = 2*self._cfg.numparts
        # Set socket timeout to a very low value
        self._cfg.socktimeout = 30.0
        # self._cfg.requests = 2*self._cfg.numparts
        if self._cfg.hgetverbose:
            self._cfg.verbosity=logger.MOREINFO
        else:
            self._cfg.verbosity = 1

        SetLogSeverity()

        self.process_plugins()
        
        self.register_common_objects()
        self.create_initial_directories()

    def hget(self):
        """ Download all URLs """

        if len(self._cfg.urls)==0:
            print 'Error: No input URL/file given. Run with -h or no arguments to see usage.\n'
            return -1

        dmgr = GetObject('datamanager')
        dmgr.initialize()
        self._pool = dmgr.get_url_threadpool()

        self._monitor = urlthread.HarvestManUrlThreadPoolMonitor(self._pool)
        self._monitor.start()
            
        for arg in self._cfg.urls:
            # Check if the argument is a file, if so
            # download URLs specified in the file.
            if os.path.isfile(arg):
                # Open it, read URL per line and schedule download
                print 'Input file %s found, scheduling download of URLs...' % arg
                try:
                    for line in file(arg):
                        line = line.strip()
                        # The line can optionally contain a different output
                        # file name, in which case it should be separated by
                        # commas...
                        items = line.split(',')
                        if len(items)==2:
                            url, filename = items[0].strip(), items[1].strip()
                            if self.grab_url(url, filename) == HGET_KEYBOARD_INTERRUPT:
                                break
                        elif len(items)==1:
                            url = items[0].strip()
                            if self.grab_url(url) == HGET_KEYBOARD_INTERRUPT:
                                break
                        print ''

                #except IOError, e:
                #    print 'Error:',e
                except Exception, e:
                    raise
            else:
                self.grab_url(arg)

        self._monitor.stop()

    def main(self):
        """ Main routine """

        # Add help option if no arguments are given
        if len(sys.argv)<2:
            sys.argv.append('-h')
            
        self.init()
        self.hget()
        return 0

if __name__ == "__main__":
    h = Hget()
    h.main()
    

import sys
sys.path.append('/home/anand/programs/python')

import mytimeit
import lrucache, lrucache2
import random

def lru1():
    l = lrucache.LRU(5000)

    # Add items
    for x in range(5000):
        l[x] = x + 1
        
    # Select some random 1000 entries
    for x in range(1000):
        i = random.randint(0, 4999)
        y = l[i]
        
    # Add additional 1000 entries...
    for x in range(1000):
        l[5000+x] = x + 1

    # Some entries will be dropped
    # Loop again...
    for x in range(5000):
        try:
            y = l[x]
        except KeyError:
            pass


def lru2():
    l = lrucache2.LRUCache(5000)

    # Add items
    for x in range(5000):
        l[x] = x + 1
        
    # Select some random 1000 entries
    for x in range(1000):
        i = random.randint(0, 5000)
        y = l[i]
        
    # Add additional 1000 entries...
    for x in range(1000):
        l[5000+x] = x + 1

    # Some entries will be dropped
    # Loop again...
    for x in range(5000):
        try:
            y = l[x]
        except lrucache2.CacheKeyError:
            pass

if __name__ == "__main__":
    print mytimeit.Timeit(lru1, 10)
    print mytimeit.Timeit(lru2, 1)    

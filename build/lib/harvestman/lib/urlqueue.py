# -- coding: utf-8
""" urlqueue.py - Module which controls queueing of urls
    created by crawler threads. This is part of the HarvestMan
    program.

    Author: Anand B Pillai <abpillai at gmail dot com>
    
    Modification History

     Anand Jan 12 2005 -   Created this module, by splitting urltracker.py
     Aug 11 2006  Anand    Checked in changes for reducing CPU utilization.

     Aug 22 2006  Anand    Changes for fixing single-thread mode.
     Oct 19 2007  Anand    Added a very basic state-machine for managing
                           crawler end condition.
     Oct 22 2007  Anand    Enhanced the state machine with additional states,
                           checks and a modified mainloop etc.

   Copyright (C) 2005 Anand B Pillai.     

"""

__version__ = '2.0 b1'
__author__ = 'Anand B Pillai'

import bisect
from Queue import *
import time

import crawler

import threading
import sys, os
import copy
import urltypes
from collections import deque

from common.common import *
from common.macros import *

from common.singleton import Singleton

class HarvestManCrawlerState(Singleton):
    """ State machine for signalling crawler end condition
    and for managing end-condition stalemates and other
    issues """

    def __init__(self, queue):
        self.ts = {}
        self.cond = threading.Condition(threading.Lock())
        self.queue = queue
        # Flags
        # All threads blocked (waiting)
        self.blocked = False
        # All fetchers blocked (waiting)
        self.fblocked = False
        # All crawlers blocked (waiting)
        self.cblocked = False
        # Crawler thread transitions
        self.ctrans = 0
        # Fetcher thread transitions
        self.ftrans = 0
        # Pushes by fetcher
        self.fpush = 0
        # Pushes by crawler
        self.cpush = 0
        # Number of crawlers
        self.numcrawlers = 0
        # Number of fetchers
        self.numfetchers = 0
        # Suspend time-stamp, initially
        # set to None. To suspend end-state
        # checking, set this to current time...
        self.st = None
        # End state message. If normal exit
        # this is not set, for abormal exit
        # this could be set...
        self.abortmsg = ''
        
    def set(self, thread, state):

        curr, role = None, None
        item = self.get(thread)
        if item:
            curr, role = item
            
        if curr != state:
            # print 'Thread %s changes from state %s to state %s' % (thread, curr, state)
            self.ts[thread.getName()] = state, thread._role

        self.state_callback(thread, state)

    def get(self, thread):
        return self.ts.get(thread.getName())

    def get_state(self):

        try:
            self.cond.acquire()
            
            # Return state for state saving
            state = {}
            state['ts'] = self.ts.copy()
            state['blocked'] = self.blocked
            state['fblocked'] = self.fblocked
            state['cblocked'] = self.cblocked        
            state['ctrans'] = self.ctrans
            state['ftrans'] = self.ftrans        
            state['fpush'] = self.fpush
            state['cpush'] = self.cpush
            state['st'] = self.st
            
            return state
        
        finally:
            self.cond.release()

    def set_state(self, state):

        # Return state for state saving
        self.ts = state.get('ts', {})
        state['ts'] = self.ts
        self.blocked = state.get('blocked', 0)
        self.fblocked = state.get('fblocked', 0)
        self.cblocked = state.get('cblocked', 0)
        self.ctrans = state.get('ctrans', 0)
        self.ftrans = state.get('ftrans', 0)
        self.fpush = state.get('fpush', 0)
        self.cpush = state.get('cpush', 0)
        self.st = state.get('st')

    def zero_thread(self):
        """ Function which returns whether any of the
        thread counts (fetcher/crawler) have gone to zero """

        return (self.numfetchers==0) or (self.numcrawlers == 0)

    def suspend(self):
        """ Suspend checks on end state. This uses a timeout
        on the suspend flag which automaticall ages the flag
        and resets if, if not set within the aging period """

        self.st = time.time()

    def unsuspend(self):
        """ Unsuspend checks on end-state. """

        self.st = None
        
    def state_callback(self, thread, state):
        """ Callbacks for taking action according to state transitions """

        self.cond.acquire()
        typ = thread._role
        
        if state == crawler.THREAD_STARTED:
            if thread.resuming:
                # Resuming threads should call unsuspend..
                self.unsuspend()
            
        # If the thread is killed, try to regenerate it...
        elif state == crawler.THREAD_DIED:
            # print 'THREAD DIED',thread
            # Don't try to regenerate threads if this is a local exception.
            e = thread.exception
            # See class for last error. If it is same as
            # this error, don't do anything since this could
            # be a programming error and will send us into
            # a loop...
            if str(thread.__class__._lasterror) == str(e):
                debug('Looks like a repeating error, not trying to restart thread %s' % (str(thread)))
                # In this case the thread has died, so reduce local thread count
                if typ=='crawler':
                    self.numcrawlers -= 1
                elif typ == 'fetcher':
                    self.numfetchers -= 1

                del self.ts[thread]
            else:
                thread.__class__._lasterror = e
                # Release the lock now!
                self.cond.release()
                # Set suspend flag
                self.suspend()
                
                del self.ts[thread]
                extrainfo('Tracker thread %s has died due to error: %s' % (str(thread), str(e)))
                self.queue.dead_thread_callback(thread)

                return 
            
        elif state == crawler.FETCHER_PUSHED_URL:
            # Push count for fetcher threads
            self.fpush += 1
        elif state == crawler.CRAWLER_PUSHED_URL:            
            # Push count for fetcher threads
            self.cpush += 1
        elif state == crawler.THREAD_SLEEPING:
            # A sleep state can be achieved only after a work state
            # so this indicates a cycle of transitions since
            # a cycle ends with a sleep...
            if typ == 'crawler':
                # Transition count for crawler threads                
                self.ctrans += 1
            elif typ == 'fetcher':
                # Transition count for crawler threads                
                self.ftrans += 1                
                
        elif state in (crawler.FETCHER_WAITING, crawler.CRAWLER_WAITING):
            if self.end_state():
                # This is useful only if the waiter is waiting
                # using wait1(...) method. If he is waiting
                # using wait2(...) method, he needs to devise
                # his own wake-up logic.
                self.cond.notify()

        self.cond.release()
            
    def end_state(self):
        """ Check end state for the program. Returns True
        if the program is ready to end. Abnormal exits are
        not handled here """

        # Time stamp of calling this function
        currt = time.time()
        # Check suspend time-stamp
        if self.st:
            # Calculate difference, do not allow suspending
            # for more than 5 seconds.
            if (currt - self.st)>5.0:
                self.st = None
            return False

        if self.zero_thread():
            self.abortmsg = "Fatal thread reduction, stopping program"
            return True
        
        flag = True
        numthreads = 0
        
        fcount, fnblock, ccount, cnblock = 0, 0, 0, 0
        self.blocked, self.fblocked, self.cblocked = False, False, False

        for state, role in self.ts.values():
            numthreads += 1

            if role == 'fetcher':
                fcount += 1
                if state == crawler.FETCHER_WAITING:
                    fnblock += 1
                else:
                    flag = False
                    break
            else:
                ccount += 1
                if state == crawler.CRAWLER_WAITING:
                    cnblock += 1
                else:
                    flag = False
                    break

         # For exit based on thread waiting state allignment
        if flag:
            self.blocked = True

        if ccount==cnblock:
            self.cblocked = True

        if fcount==fnblock:
            self.fblocked = True

        if self.blocked:
            # print 'Length1 => ',len(self.queue.data_q)
            # print 'Length2 => ',len(self.queue.url_q)
            
            #print "Pushes=>",self.queue._pushes
            # print 'Transitions',self.ctrans, self.ftrans,self.fpush,self.cpush

            # If we have one fpush event, we need to have at least
            # one cpush associated to it...
            if self.fpush>0:
                return (self.cpush>0)

            return True

        return False

    def __str__(self):
        return str(self.ts)

    def wait1(self, timeout):
        """ Regular wait method. This should be typically
        called with a large timeout value """

        while not self.end_state():
            self.cond.acquire()
            try:
                self.cond.wait(timeout)
            except IOError, e:
                break
            
            self.cond.release()
        
    def wait2(self, timeout):
        """ Secondary wait method. This should be typically
        called with a small timeout value. When calling
        this the caller should have additional logic to
        make sure he does not time out before the condition
        is met """
        
        try:
            self.cond.acquire()
            self.cond.wait(timeout)
            self.cond.release()
        except IOError, e:
            pass
        
class PriorityQueue(Queue):
    """ Priority queue based on bisect module (courtesy: Effbot) """

    def __init__(self, maxsize=0):
        Queue.__init__(self, maxsize)

    def _init(self, maxsize):
        self.maxsize = maxsize
        self.queue = []
        
    def _put(self, item):
        bisect.insort(self.queue, item)

    def __len__(self):
        return self.qsize()
    
    def _qsize(self):
        return len(self.queue)

    def _empty(self):
        return not self.queue

    def _full(self):
        return self.maxsize>0 and len(self.queue) == self.maxsize

    def _get(self):
        return self.queue.pop(0)    

class HarvestManCrawlerQueue(object):
    """ This class functions as the thread safe queue
    for storing url data for tracker threads """

    def __init__(self):
        self._basetracker = None
        self._controller = None # New in 1.4
        self._flag = 0
        self._pushes = 0
        self._lasttimestamp = time.time()
        self._trackers  = []
        self._savers = []
        self._requests = 0
        self._trackerindex = 0
        self._baseUrlObj = None
        self.stateobj = HarvestManCrawlerState(self)

        self._configobj = GetObject('config')
        self.url_q = PriorityQueue(self._configobj.queuesize)
        self.data_q = PriorityQueue(self._configobj.queuesize)
            
        # Local buffer
        self.buffer = []
        # Lock for creating new threads
        self._cond = threading.Lock()
        # Flag indicating a forceful exit
        self.forcedexit = False
        # Sleep event
        self.evnt = SleepEvent(self._configobj.queuetime)

    def get_state(self):

        # Set flag
        self._flag = 1
        
        # Return state of this object,
        # it's queues and the threads it contain
        d = {}
        d['_pushes'] = self._pushes
        d['_lasttimestamp'] = self._lasttimestamp
        d['_requests'] = self._requests
        d['buffer'] = self.buffer
        d['_baseUrlObj'] = self._baseUrlObj
        d['stateobj'] = self.stateobj.get_state()
        
        # For the queues, get their contents
        q1 = self.url_q.queue
        # This is an index of priorities and url indices
        d['url_q'] = q1
        q2 = self.data_q.queue
        d['data_q'] = q2

        # Thread dictionary
        tdict = {}
        
        # For threads get their information
        for t in self._trackers:
            d2 = {}
            d2['_loops'] = t._loops          
            
            d2['_url'] = t._url
            d2['_urlobject'] = t._urlobject
            d2['buffer'] = t.buffer
            d2['role'] = t._role
            if t._role == 'crawler':
                d2['links'] = t.links
            elif t._role == 'fetcher':
                pass

            tdict[t._index] = d2
            
        d['threadinfo'] = tdict

        dcopy = copy.deepcopy(d)

        return dcopy
        
    def set_state(self, state):
        """ Set state to a previous saved state """

        # Get base url object
        self._baseUrlObj = state.get('_baseUrlObj')
        # If base url object is None, we cannot proceed
        if self._baseUrlObj is None:
            return SET_STATE_ERROR
        
        # Set state for simple data-members
        self._pushes = state.get('_pushes',0)
        self._lasttimestamp = state.get('_lasttimestamp', time.time())
        self._requests = state.get('_requests', 0)
        self.buffer = state.get('buffer', [])
        stateobj_state = state.get('stateobj', None)
        # State object's state
        if stateobj_state:
            self.stateobj.set_state(stateobj_state)
        
        # Set state for queues
        self.url_q.queue = state.get('url_q', [])
        
        self.data_q.queue = state.get('data_q', [])

        # If both queues are empty, we don't have anything to do
        if len(self.url_q.queue)==0 and len(self.data_q.queue)==0:
            moreinfo('Size of data/url queues are zero, nothing to re-run')
            return SET_STATE_ERROR
        
        cfg = GetObject('config')
        self._configobj = cfg
        
        if cfg.fastmode:
            # Create threads and set their state
            for idx,tdict in state.get('threadinfo').items():
                role = tdict.get('role')
                t = None
                
                if role == 'fetcher':
                    t = crawler.HarvestManUrlFetcher(idx, None)
                    self.stateobj.numfetchers += 1
                elif role == 'crawler':
                    t = crawler.HarvestManUrlCrawler(idx, None)
                    t.links = tdict.get('links')
                    self.stateobj.numcrawlers += 1

                if t:
                    t._loops = tdict.get('_loops')
                    t._url = tdict.get('_url')
                    t._urlobject = tdict.get('_urlobject')
                    t.buffer = tdict.get('buffer')
                    if t._urlobject: t.resuming = True
                    
                    self.add_tracker(t)
                    t.setDaemon(True)

            # Set base tracker
            self._basetracker = self._trackers[0]

        return SET_STATE_OK
    
    def get_controller(self):
        """ Return the controller thread object """

        return self._controller
    
    def configure(self):
        """ Configure this class with this config object """

        try:
            import urlparser
            urlparser.HarvestManUrlParser.seed_index()
            
            self._baseUrlObj = urlparser.HarvestManUrlParser(self._configobj.url,
                                                             urltypes.URL_TYPE_ANY,
                                                             0, self._configobj.url,
                                                             self._configobj.projdir)
            dmgr = GetObject('datamanager')
            dmgr.add_url(self._baseUrlObj)
        except urlparser.HarvestManUrlParserError:
            return False

        self._baseUrlObj.starturl = True
        
        if self._configobj.fastmode:
            try:
                self._basetracker = crawler.HarvestManUrlFetcher( 0, self._baseUrlObj, True )
            except Exception, e:
                print "Fatal Error:",e
                hexit(1)
                
        else:
            # Disable usethreads
            self._configobj.usethreads = False
            # Disable blocking
            self._configobj.blocking = False
            self._basetracker = crawler.HarvestManUrlDownloader( 0, self._baseUrlObj, False )
            
        self._trackers.append(self._basetracker)
        return True

    def mainloop(self):
        """ Main program loop which waits for
        the threads to do their work """

        # print 'Waiting...'
        timeslot, tottime = 0.5, 0
        pool = GetObject('datamanager').get_url_threadpool()
        
        while not self._flag:
            # print 'Waiting...'
            self.stateobj.wait2(timeslot)
            if self.stateobj.end_state():
                # Check for worker thread pool...
                # with a timeout of 5 minutes
                extrainfo("Waiting for pool...")
                pool.wait(10.0, self._configobj.timeout)
                extrainfo("Pool is free now...")                
                break

            # print 'Checking...'
            tottime += timeslot
            #if self._flag:
            #    break

        if self.stateobj.abortmsg:
            extrainfo(self.stateobj.abortmsg)
            
        # print 'I got woken up!',threading.currentThread()
        if self._flag != 1:
            self.endloop()

    def endloop(self, forced=False):
        """ Exit the mainloop """

        # Set flag to 1 to denote that downloading is finished.
        self._flag = 1
        if forced:
            self.forcedexit = True
        self.end_threads()

    def restart(self):
        """ Alternate method to start from a previous restored state """

        # Start harvestman controller thread
        import datamgr
        
        self._controller = datamgr.HarvestManController()
        self._controller.start()

        # Start base tracker
        self._basetracker.start()
        time.sleep(2.0)

        for t in self._trackers[1:]:
            try:
                t.start()
            except AssertionError, e:
                logconsole(e)
                pass

        self.mainloop()
        
    def crawl(self):
        """ Starts crawling for this project """

        # Reset flag
        self._flag = 0

        t1=time.time()

        # Set start time on config object
        self._configobj.starttime = t1

        # Push the first URL directly to the url queue
        self.url_q.put((self._baseUrlObj.priority, self._baseUrlObj))

        if self._configobj.fastmode:

            # Start harvestman controller thread
            import datamgr
            
            self._controller = datamgr.HarvestManController()
            self._controller.start()

            #import filethread
            
            # Create two filesaver threads
            #for x in range(2):
            #    t = filethread.HarvestManFileThread()
            #    self._savers.append(t)
            #    t.setDaemon(True)
            #    t.start()
                
            # Create the number of threads in the config file
            # Pre-launch the number of threads specified
            # in the config file.

            # Initialize thread dictionary
            self.stateobj.numfetchers = self._configobj.maxtrackers/2 + self._configobj.maxtrackers % 2
            self.stateobj.numcrawlers = self._configobj.maxtrackers/2 
            
            self._basetracker.setDaemon(True)
            self._basetracker.start()

            evt = SleepEvent(0.1)
            while self.stateobj.get(self._basetracker) == crawler.FETCHER_WAITING:
                evt.sleep()

            for x in range(1, self._configobj.maxtrackers):

                if x % 2==0:
                    t = crawler.HarvestManUrlFetcher(x, None)
                else:
                    t = crawler.HarvestManUrlCrawler(x, None)

                self.add_tracker(t)
                t.setDaemon(True)
                t.start()

            self.mainloop()
        else:
            self._basetracker.action()

    def get_base_tracker(self):
        """ Get the base tracker object """

        return self._basetracker

    def get_base_urlobject(self):

        return self._baseUrlObj
    
    def get_url_data(self, role):
        """ Pop url data from the queue """

        if self._flag: return None
        
        obj = None

        blk = self._configobj.blocking

        slptime = self._configobj.queuetime
        ct = threading.currentThread()
        
        if role == 'crawler':
            if blk:
                obj=self.data_q.get()
            else:
                self.stateobj.set(ct, crawler.CRAWLER_WAITING)
                try:
                    obj = self.data_q.get(timeout=slptime)
                except Empty, TypeError:
                    obj = None
                    
        elif role == 'fetcher' or role=='tracker':
            
            if blk:
                obj = self.url_q.get()
            else:
                try:
                    self.stateobj.set(ct, crawler.FETCHER_WAITING)
                    obj = self.url_q.get(timeout=slptime)
                except Empty, TypeError:
                    obj = None
            
        self._lasttimestamp = time.time()        

        self._requests += 1
        return obj

    def add_tracker(self, tracker):
        self._trackers.append( tracker )
        self._trackerindex += 1

    def remove_tracker(self, tracker):
        self._trackers.remove(tracker)

    def dead_thread_callback(self, t):
        """ Call back function called by a thread if it
        dies with an exception. This class then creates
        a fresh thread, migrates the data of the dead
        thread to it """

        
        try:
            debug('Trying to regenerate thread...')
            self._cond.acquire()
            # First find out the type
            role = t._role
            new_t = None

            if role == 'fetcher':
                new_t = crawler.HarvestManUrlFetcher(t.get_index(), None)
            elif role == 'crawler':
                new_t = crawler.HarvestManUrlCrawler(t.get_index(), None)

            # Migrate data and start thread
            if new_t:
                new_t._url = t._url
                new_t._urlobject = t._urlobject
                
                new_t.buffer = copy.deepcopy(t.buffer)
                # If this is a crawler get links also
                if role == 'crawler':
                    new_t.links = t.links[:]
                    
                # Replace dead thread in the list
                idx = self._trackers.index(t)
                self._trackers[idx] = new_t
                new_t.resuming = True
                new_t.start()

                debug('Regenerated thread...')
                
                return THREAD_MIGRATION_OK
            else:
                # Could not make new thread, so decrement
                # count of threads.
                # Remove from tracker list
                self._trackers.remove(t)
                
                if role == 'fetcher':
                    self.stateobj.numfetchers -= 1
                elif role == 'crawler':
                    self.stateobj.numcrawlers -= 1

                return THREAD_MIGRATION_ERROR
        finally:
            self._cond.release()
                
    def push(self, obj, role):
        """ Push trackers to the queue """

        if self._flag: return
        
        ntries, status = 0, 0
        ct = threading.currentThread()
        
        if role == 'crawler' or role=='tracker' or role =='downloader':
            # debug('Pushing stuff to buffer',ct)
            self.stateobj.set(ct, crawler.CRAWLER_PUSH_URL)
            
            while ntries < 5:
                try:
                    ntries += 1
                    self.url_q.put((obj.priority, obj))
                    self._pushes += 1
                    status = 1
                    self.stateobj.set(ct, crawler.CRAWLER_PUSHED_URL)
                    break
                except Full:
                    self.evnt.sleep()
                    
        elif role == 'fetcher':
            # print 'Pushing stuff to buffer', ct
            self.stateobj.set(ct, crawler.FETCHER_PUSH_URL)                                
            # stuff = (obj[0].priority, (obj[0].index, obj[1]))
            while ntries < 5:
                try:
                    ntries += 1
                    self.data_q.put(obj)
                    self._pushes += 1
                    status = 1
                    self.stateobj.set(ct, crawler.FETCHER_PUSHED_URL)                    
                    break
                except Full:
                    self.evnt.sleep()                    

        self._lasttimestamp = time.time()

        return status
    
    def end_threads(self):
        """ Stop all running threads and clean
        up the program. This function is called
        for a normal/abnormal exit of HravestMan """

        moreinfo("Ending threads...")
        if self._configobj.project:
            if self.forcedexit:
                moreinfo('Terminating project ',self._configobj.project,'...')
            else:
                moreinfo("Ending Project", self._configobj.project,'...')

        # Stop controller
        self._controller.stop()

        # Stop savers
        #for t in self._savers:
        #    t.stop()

        if self.forcedexit:
            self._kill_tracker_threads()
        else:
            # Do a regular stop and join
            for t in self._trackers:
                try:
                    t.stop()
                except Exception, e:
                    pass
            

        self._trackers = []
        self._basetracker = None

    def _kill_tracker_threads(self):
        """ This function kills running tracker threads """

        count =0

        for tracker in self._trackers:
            count += 1
            sys.stdout.write('...')

            if count % 10 == 0: sys.stdout.write('\n')

            try:
                tracker.stop()
            except AssertionError, e:
                logconsole(str(e))
            except ValueError, e:
                logconsole(str(e))



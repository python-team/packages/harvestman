from pyparsing import *

# script_open = Literal("<") + Literal("script") + OneOrMore(Word(alphas) + Literal('=') + Word(alphanums + '"' + ".")) + Literal(">")
script_open = Literal("<") + Literal("script") + OneOrMore(Word(alphas) + Literal("=") + Word(alphanums + "."+ "/"  + '"'))  + Literal(">") + SkipTo(Literal("</") + Literal("script") + Literal(">"))

s1='anand <script language="JavaScript1.2" type="text/javascript" >'
s2='<script language="javascript">function(){}</script>'
s3='<script language="Javascript1.2">'
s4="<html><body><p>Some text</p></body></html>"

data = open("samples/jstest3.html").read()

#print dir(script_open)
#for s in (s1,s2,s3,s4):
#    print script_open.searchString(s)

for item in script_open.scanString(data):
    print item
#script_open.setParseAction(replaceWith(''))
#print script_open.transformString(open('samples/jstest.html').read())
#print script_open.transformString(s1)
